/**
 * Load a std::vector from a file.
 * WARNING: The user has to provide the correct number of entries.
 */

#include <fstream>
#include <vector>
#include <iterator>

#include <iostream>
#include <iomanip>

struct key_value {
  int32_t key1_; // instrument number
  int32_t key2_; // price (>0)
  int32_t value_; // quantity
};

typedef std::vector<key_value> data_t;

int main(int argc, char * argv[])
{
    if (argc != 3)
    {
        std::cerr << "Usage: " << argv[0] << " <file> <len>" << std::endl;
        return 1;
    }

    int len = std::stoi(argv[2]);

    std::ifstream infile;
    infile.open(argv[1], std::fstream::in | std::fstream::binary);
    if (infile.fail())
    {
      std::cerr << "Opening file '" << argv[1] << "' failed!" << std::endl;
      return 1;
    }

    data_t data(len);

    infile.read(reinterpret_cast<char*>(data.data()), len*sizeof(key_value));
    infile.close();

    for (key_value & kv : data)
        std::cout
            << std::setw(6) << kv.key1_
            << std::setw(6) << kv.key2_
            << std::setw(6) << kv.value_
            << std::endl;

    return 0;
}
