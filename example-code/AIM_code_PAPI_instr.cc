/**
 * This the same code as the original AIM_code.cc but with PAPI calls
 * instead of std::chrono to get the number of cycles and instructions
 * spent by the CPU in our to-be-optimized section.
 *
 * Compile with (once you have installed PAPI, see our project wiki)
 *   g++ -O3 --std=c++11 AIM_code_PAPI_instr AIM_code_PAPI_instr.cc -lpapi
 *
 * If you have built PAPI but not installed it anywhere, use '-L ...' to specify
 * the directory where the library is. If you have only a static libpapi.a, use
 * '-static' to build a static binary.
 */

#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <map>
#include <random>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>

#include <papi.h>

// run with: g++ -O3 --std=c++11 ./example.cpp -o example && ./example

struct random_number_generators
{
  int32_t key1() { return int(key1_distribution_(gen_)); }

  int32_t lowest() { return 100 + int(std::abs(lowest_distribution_(gen_))); }
  int32_t std_dev() { return 2 + std::abs(std_dev_distribution_(gen_)); }

  bool should_delete(int32_t value) { return real_distribution_(gen_) < value; }
  std::random_device device_{};

  std::mt19937 gen_{device_()};

  std::normal_distribution<double> key1_distribution_{1000, 100};
  std::normal_distribution<double> lowest_distribution_{0, 1000};
  std::normal_distribution<double> std_dev_distribution_{0, 10};
  std::uniform_real_distribution<double> real_distribution_{0, 10};
};

struct key2_gen_state
{
  key2_gen_state(random_number_generators& rngs) :
    rngs_(rngs),
    lowest_(rngs.lowest()),
    std_dev_(rngs.std_dev()), key2_distribution_(0, std_dev_)
  {}

  int32_t next_value() { return lowest_ + int(std::abs(key2_distribution_(rngs_.gen_))); }

  random_number_generators& rngs_;
  int32_t lowest_;
  int32_t std_dev_;
  std::normal_distribution<double> key2_distribution_;
};

struct key_value {
  int32_t key1_; // instrument number
  int32_t key2_; // price (>0)
  int32_t value_; // quantity
};

std::vector<key_value> generate_test_data(size_t count) {
  random_number_generators rngs;
  std::map<int32_t, key2_gen_state> key2_generators;
  std::vector<key_value> test_data;
  typedef std::map<int32_t, std::map<int32_t, int32_t>> map_type_t;
  map_type_t data;
  int64_t running_total = 0;

  for (size_t i = 0; i < count; ++i)
  {
    int32_t key1 = rngs.key1();
    auto key2_gen_iter = key2_generators.find(key1);

    if (key2_gen_iter == key2_generators.end())
      key2_gen_iter = key2_generators.insert({key1, key2_gen_state{rngs}}).first;

    int32_t key2 = key2_gen_iter->second.next_value();
    auto entry = data.find(key1);

    if (entry == data.end())
      entry = data.insert({key1, {}}).first;

    auto insert_result = entry->second.insert({key2, 0});
    int32_t value = 1;

    if (insert_result.second)
    {
      insert_result.first->second = value;
    }
    else if (entry->second.size() > 1 && rngs.should_delete(insert_result.first->second))
    {
      value = -(insert_result.first->second);
      entry->second.erase(insert_result.first);
    }
    else
    {
      insert_result.first->second += value;
    }

    for (auto kv : entry->second) {}

    running_total += entry->second.begin()->second;

    test_data.push_back({key1, key2, value});
  }

  std::cout << "expected total: " << running_total << std::endl;
  return test_data;
}

void handle_error(int retval)
{
  std::cerr << "PAPI error " << retval << ": " << PAPI_strerror(retval) << std::endl;
  exit(1);
}

int main() {
  // get test data
  std::cout << "generating test data..." << std::endl;
  auto test_data = generate_test_data(100);

  // run the algorithm, record the timings.

  // this is the bit you should try to optimise.

  std::cout << "running..." << std::endl;

  const static size_t NUM_EVENTS = 3;
  int Events[NUM_EVENTS] = { PAPI_TOT_CYC, PAPI_TOT_INS, PAPI_REF_CYC };
  int num_hwcntrs = 0;
  long_long values[NUM_EVENTS];

  if ((num_hwcntrs = PAPI_num_counters()) <= PAPI_OK)  
    handle_error(1);

  std::cout << "This system has " << num_hwcntrs << " available counters" << std::endl;
  
  // limit the number of hw counters to what we actually need
  if (num_hwcntrs > 2)
    num_hwcntrs = NUM_EVENTS;

  // Start counting events
  if (PAPI_start_counters(Events, num_hwcntrs) != PAPI_OK)
    handle_error(1);

  auto s = PAPI_get_real_usec();

  int64_t sum = 0;
  typedef std::map<int32_t, std::map<int32_t, int32_t>> map_type_t;
  map_type_t data;

  // kv is a reference to a key_value instance
  for (auto& kv : test_data)
  {
    // lookup an instrument/stock, get an iterator
    auto entry = data.find(kv.key1_);

    // create an entry if not found
    if (entry == data.end())
      // map::insert() returns a tuple (iterator, bool), where .second is true
      // if this new element was actually inserted, otherwise false
      // entry is set to the newly created instance if it was data.end() before
      entry = data.insert({kv.key1_, {}}).first;

    // insert an entry with quantity 0 for this price if none exists for kv.key2_
    auto insert_result = entry->second.insert({kv.key2_, 0});

    // create a shortcut for the quantiy
    auto& value_ref = insert_result.first->second;

    // add the new quantity
    if ((value_ref += kv.value_) == 0)
      // .. and remove this quote if the quantity is 0
      entry->second.erase(insert_result.first);

    sum += entry->second.begin()->second;
  }

  auto e = PAPI_get_real_usec();

  if (PAPI_stop_counters(values, NUM_EVENTS) != PAPI_OK)
    handle_error(1);

  std::cout << e-s << std::endl;

  std::cout
    << "test data size: " << test_data.size()
    << ", total cycles: " << values[0]
    << ", ref cycles: " << values[2]
    << ", total instructions: " << values[1]
    << ", sum: " << sum
    << std::endl;

  return 0;

}
