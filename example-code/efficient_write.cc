/**
 * A way to efficiently write a vector to a file
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>

struct key_value {
  int32_t key1_; // instrument number
  int32_t key2_; // price (>0)
  int32_t value_; // quantity
};

typedef std::vector<key_value> data_t;

int main(int argc, char * argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " <file>" << std::endl;
        return 1;
    }

    std::ofstream outfile;
    outfile.open(argv[1], std::fstream::out | std::fstream::binary | std::fstream::trunc);
    if (outfile.fail())
    {
      std::cerr << "Opening file '" << argv[1] << "' failed!" << std::endl;
      return 1;
    }

    data_t data;

    data.push_back({1,2,3});
    data.push_back({5,6,7});
    data.push_back({8,9,10});
    data.push_back({11,12,13});

    std::cout << "Data =>" << std::endl;

    for (key_value & kv : data)
        std::cout
            << std::setw(6) << kv.key1_
            << std::setw(6) << kv.key2_
            << std::setw(6) << kv.value_
            << std::endl;

    std::cout
      << "<=" << std::endl
      << "# of entries: " << data.size()
      << std::endl;

    outfile.write(reinterpret_cast<char*>(data.data()), data.size()*sizeof(key_value));
    outfile.close();

    return 0;
}
