#include <chrono>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <map>
#include <random>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <cassert>
#include "effio.h"
#include "portfolio.h"
// run with: g++ -O3 --std=c++11 ./example.cpp -o example && ./example


double portfolio(int num_entries, const char * filename) {
  /*if (argc != 3)
  {
    std::cerr << "Usage: " << argv[0] << " <number of entries> <filename>" << std::endl;
    return 1;
  }*/

  // Read test data from file
  int SampleDataLength = num_entries;
  data_t test_data(SampleDataLength);
  readDataFromFile(filename, test_data, SampleDataLength);

    std::cout << "running..." << std::endl;

    const static uint32_t LARGEST_STOCK = 2048;
    const static uint32_t STOCK_SHIFT = 13; // how many bits we want to reserve for the price
    const static uint32_t LARGEST_PRICE = 1 << STOCK_SHIFT;

    std::vector<char> quantities(LARGEST_STOCK*LARGEST_PRICE, 0);
    std::vector<uint16_t> lowest_prices(LARGEST_STOCK, LARGEST_PRICE); // initialize with the largest price we can get
    int64_t sum = 0;

    const auto begin = std::chrono::high_resolution_clock::now();

    // kv is a reference to a key_value instance
    for (auto& kv : test_data)
    {
      //assert(kv.key1_ < PRICE_STRIDE && "key1_ not representable with specified number of bytes");
      assert(kv.key2_ < LARGEST_PRICE && "key2_ not representable with specified number of bytes");

      uint64_t key = (uint64_t(kv.key1_) << STOCK_SHIFT) + uint64_t(kv.key2_);
      // lookup by price and instrument
      char & quantity = quantities[key];

      quantity += kv.value_;

      uint16_t & lowest_price = lowest_prices[kv.key1_];

      // TODO: the following decision logic could be optimized
      // further by a) doing the summation in the branches directly
      // and b) by caching also the quantity for the lowest price.
      // Furthermore it would be adviseable to also store the second
      // lowest price to be able to calculate the required answers first
      // and then do the maintenance work (finding the next second lowest
      // price in our case).

      // we get a new lowest price and since we only get non-zero
      // quantities in the quotes we can simply reset the lowest price
      if (kv.key2_ < lowest_price)
        lowest_price = kv.key2_;
      else if ((kv.key2_ == lowest_price) && (quantity == 0))
      {
        // this is the most expensive case:
        // we just set the available quantities for the current lowest price to zero
        // and now we have to find the next lowest price for which there is a positive quantity

        // we can directly start with the next price
        uint64_t new_lowest_price_key_offset = 1;

        // we are guaranteed to have always at least one other quote in there
        // and we have to stride over the prices array,
        // where the faster index is the stock identifier
        // the offset is based on the shift of the prices by N bytes for the combined index
        while (!quantities[new_lowest_price_key_offset + key])
          new_lowest_price_key_offset += 1;

        // the lowest price is the upper half of the N bit key
        // (remember: combined price/stock id key)
        lowest_price = kv.key2_ + new_lowest_price_key_offset;
      }

      sum += quantities[(uint64_t(kv.key1_) << STOCK_SHIFT) + lowest_price];
    }

    const auto end = std::chrono::high_resolution_clock::now();

    std::cout
      << "test data size: " << test_data.size()
      << " duration: " << std::chrono::duration_cast<std::chrono::duration<double>>(end - begin).count() << "ms,"
      << " sum: " << sum
      << std::endl;

    return  std::chrono::duration_cast<std::chrono::duration<double>>(end - begin).count();

  }
