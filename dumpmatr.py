#!/home/niccolo/Programs/miniconda2/bin/python

import math as m
import numpy as np
#import random as ran
import optparse, random, pandas
import os, re, sys
import matplotlib.pyplot as plt

#from numba import jit

from IPython import embed
import matplotlib

plt.xkcd()

matplotlib.rcParams.update({'font.size': 30})
matplotlib.rcParams['figure.figsize'] = 10 , 4

resNS = np.array(pandas.io.parsers.read_csv('mfileNS.csv',delim_whitespace=True,header=None))
resSH = np.array(pandas.io.parsers.read_csv('mfileSH.csv',delim_whitespace=True,header=None))

hardl = 2**10

plt.figure().patch.set_facecolor('black')
plt.gca().set_axis_bgcolor('black')

plt.imshow(resNS[750:1250,:hardl],cmap='Greys_r')

plt.axes().get_xaxis().set_ticks([])
plt.axes().get_yaxis().set_ticks([])

plt.savefig('matr_ch_pm_NOSH.pdf',facecolor='black')
plt.clf()

plt.figure().patch.set_facecolor('black')
plt.gca().set_axis_bgcolor('black')

plt.imshow(resSH[750:1250,:hardl],cmap='Greys_r')

plt.axes().get_xaxis().set_ticks([])
plt.axes().get_yaxis().set_ticks([])

plt.savefig('matr_ch_pm_SH.pdf',facecolor='black')
plt.clf()

embed()