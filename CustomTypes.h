#include <map>
#include <random>
#include <cstdint>
#include <vector>
#include <cstdlib>

struct key_value {
  int32_t key1_; // instrument number
  int32_t key2_; // price (>0)
  int32_t value_; // quantity
};

struct random_number_generators
{
  int32_t key1() { return int(key1_distribution_(gen_)); }

  int32_t lowest() { return 100 + int(std::abs(lowest_distribution_(gen_))); }
  int32_t std_dev() { return 2 + std::abs(std_dev_distribution_(gen_)); }

  bool should_delete(int32_t value) { return real_distribution_(gen_) < value; }
  std::random_device device_{};

  std::mt19937 gen_{device_()};

  std::normal_distribution<double> key1_distribution_{1000, 100};
  std::normal_distribution<double> lowest_distribution_{0, 1000};
  std::normal_distribution<double> std_dev_distribution_{0, 10};
  std::uniform_real_distribution<double> real_distribution_{0, 10};
};

struct key2_gen_state
{
  key2_gen_state(random_number_generators& rngs) :
    rngs_(rngs),
    lowest_(rngs.lowest()),
    std_dev_(rngs.std_dev()), key2_distribution_(0, std_dev_)
  {}

  int32_t next_value() { return lowest_ + int(std::abs(key2_distribution_(rngs_.gen_))); }

  random_number_generators& rngs_;
  int32_t lowest_;
  int32_t std_dev_;
  std::normal_distribution<double> key2_distribution_;
};

typedef std::map<int32_t, std::map<int32_t, int32_t>> map_type_t;

typedef std::vector<key_value> data_t;

typedef std::map<int32_t, std::map<int32_t, int32_t>> map_type_t;
