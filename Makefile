OFLAGS	  = -O3 --std=c++11
PROFFLAGS = -g -pg
CC	  = g++
CFILES	  = ./effio.cc
HEADERS = CustomTypes.h effio.h

all: oBookFixedSize DataGen

DataGen: DataGen.cc $(HEADERS) $(CFILES)
	$(CC) $(OFLAGS) ./DataGen.cc $(CFILES) -o DataGen

oBook: DataProc.cc $(HEADERS) $(CFILES)
	$(CC) $(OFLAGS) ./DataProc.cc $(CFILES) -o oBook

oBookFixedSize: DataProcFixedSize.cc $(HEADERS) $(CFILES)
	$(CC) $(OFLAGS) -DNDEBUG ./DataProcFixedSize.cc $(CFILES) -o oBookFixedSize

obook_with_prof: DataProc.cc $(HEADERS) $(CFILES)
	$(CC) $(OFLAGS) $(PROFFLAGS) ./DataProc.cc $(CFILES) -o oBook

FS_with_prof: DataProcFixedSize.cc $(HEADERS) $(CFILES)
	$(CC) $(OFLAGS) $(PROFFLAGS) ./DataProcFixedSize.cc $(CFILES) -o oBookFixedSize

clean:
	rm -f DataGen oBook *.h.gchi

cleandata:
	rm -f DataGen *.h.gch

cleanobook:
	rm -f oBook oBookFixedSize *.h.gch
