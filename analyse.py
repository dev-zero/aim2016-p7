#!/home/niccolo/Programs/miniconda2/bin/python

import math as m
import numpy as np
#import random as ran
import optparse, random, pandas
import os, re, sys
import matplotlib.pyplot as plt

#from numba import jit

from IPython import embed
import matplotlib

plt.xkcd()

matplotlib.rcParams.update({'font.size': 30})
matplotlib.rcParams['figure.figsize'] = 15, 11

usage = "usage: %prog [options] distr_file \n it shows the distr of the variable in distr_file "

parser = optparse.OptionParser(usage)

parser.add_option('-C','--comments',default = '#', dest='COMM',help='set manually the tags for commented lines (default = #)')

parser.add_option('-n','--ntot',default = -1 , dest='ntot', type ='int',help='set manually max length of file')

parser.add_option('-i','--interactive',action="store_true", default=False, dest='INTER',help='enter in interactive mode at the end')
parser.add_option('-p','--plot',action="store_false", default=True, dest='PLOT',help='plot everything')

opts, fileargs = parser.parse_args()

filename = fileargs[0]

print 'loading %s' % filename

# @jit
#fil = np.loadtxt(filename,dtype='int')
fil = np.array(pandas.io.parsers.read_csv(filename,delim_whitespace=True,header=None))

if (opts.ntot > 0 ):
  print 'Less statistic: %d' % opts.ntot
  fil=fil[:opts.ntot]

key1s = fil[:,0]
key2s = fil[:,1]
values = fil[:,2]

k1uniq = np.unique(key1s)

k1unori = k1uniq.copy()

print '\nLoaded %d trades related to %d IDs\n' % (len(fil),len(k1uniq))

# @jit
def getid(k1,k2 = 0):
    res= fil[fil[:,0] == k1]
    if (k2 <= 0 ): return res
    return res[res[:,1] == k2]

# @jit
nerase = 0 

DOWARMUP = 1

wmupvalue = 300

class stock:
        def __init__(self,trd):
                self.k1  = trd[0]
                self.k2m = {}
		self.nt = 0
		self.recpricemin = 10000
		self.realmin = 10000 #At the end will be the minimum ever reached
		self.lastminupdate = 0 #At the end will be the last nt where we update the real minimum (for worm up)
		self.maxmin = 0 #At the end will be the maximum minimum without zero value after the wormup
        def update(self,trd):
	    self.nt += 1
            if self.k2m.has_key(trd[1]):
                self.k2m[trd[1]] += trd[2]
                if self.k2m[trd[1]] == 0:
                    del self.k2m[trd[1]]
                    global nerase
		    nerase +=1
            else:
                self.k2m[trd[1]] = trd[2]
            minim = self.pricemin()
            if self.realmin> minim:
                self.lastminupdate = self.nt
            self.realmin = min(self.realmin, minim)
            if DOWARMUP and self.nt <= wmupvalue:
	        self.recpricemin = min(self.recpricemin,minim)
	    else:
	        self.maxmin = max(self.maxmin,minim)
            #if DOWARMUP and self.nt <= wmupvalue:
	      #self.recpricemin = min(self.recpricemin,self.pricemin())
        def valmin(self):
            return self.k2m[min(self.k2m.keys())]
	def pricemin(self):
	    return  min(self.k2m.keys())
	def k2s(self):
	  return self.k2m.keys()


mappk1 = {}

# @jit
def fillmappk1(trd):
    if not mappk1.has_key(trd[0]):
        mappk1[trd[0]] = stock(trd)
    #mappk1[trd[0]].k2v = np.vstack(( mappk1[trd[0]].k2v ,trd[1:]))
    mappk1[trd[0]].update(trd)

# @jit
def main(qq=-1):
    smm = 0
    counter = 0
    for el in fil:
	counter += 1
        fillmappk1(el)
        #print el[0]
        #print mappk1[el[0]].k2v[:,0].argmin()
        smm += mappk1[el[0]].valmin() 
        #mappk1[el[0]].k2m[mappk1[el[0]].k2m[:,0].argmin(),1]
        #print mappk1[el[0]].k2v[mappk1[el[0]].k2v[:,0].argmin(),1]
        if (qq>0 and counter == qq): break
    print smm
    

def rndId():
    return random.choice(k1uniq)

''' kk=1 : key2, kk=2 : value  '''
def histid(idd,kk=1,**kwargs):
    ''' kk=1 : key2, kk=2 : value  '''
    plt.hist(getid(idd)[:,kk],**kwargs)
    #plt.show()

# @jit
''' kk=1 : key2, kk=2 : value  '''
def allstd(kk=1):
    ''' kk=1 : key2, kk=2 : value  '''
    varr = []
    for k1 in k1uniq:
        varr.append(getid(k1)[:,kk].std())
    return np.array(varr)

def allsize(kk=1):
    ''' kk=1 : key2, kk=2 : value  '''
    varr = []
    for k1 in k1uniq:
        varr.append(getid(k1)[:,kk].max() - getid(k1)[:,kk].min())
    return np.array(varr)

def givebulk(arr,perc=1.):
  reqocc = max(int(perc*arr.size),1) 
  arr_ = np.sort(arr)[:reqocc]
  return arr_.max()-arr_.min()
  

def allsizeperc(kk=1,perc=1.):
    varr = []
    for k1 in k1uniq:
        varr.append(givebulk(getid(k1)[:,kk],perc))
    return np.array(varr)


def getTopId(pr):
  ordr = {}
  for k1 in k1uniq:
    ordr[k1] = mappk1[k1].nt
    #ordrd[k1] = 
  srt = np.array(sorted(ordr, key=ordr.get))[::-1]
  return srt[pr]

def filterstocks(pr):
  ordr = {}
  global k1uniq
  for k1 in k1uniq:
    ordr[k1] = mappk1[k1].nt
    #ordrd[k1] = 
  srt = np.array(sorted(ordr, key=ordr.get))[::-1]
  #return srt[:pr]
  allids = k1uniq.size
  k1uniq = srt[:pr]
  print 'Deleted %d stocks with %d top stocks' % (allids-k1uniq.size, pr)
  return k1uniq

def allmins():
  res = []
  for k1 in k1uniq:
    res.append(mappk1[k1].pricemin())
  return np.array(res)
#gog = mappk1[getTopId]

#It returns for each key1 the difference between minimum minimum and maximum minimum after worm up (the max of this array should be the length of our arrays)
def alldiffmaxminminpr():
    varr = []
    for k1 in k1uniq:
	diff = mappk1[k1].maxmin-mappk1[k1].recpricemin
        if (diff >= 0): varr.append(diff)
    maxim= max(varr) if len(varr) > 1 else 0
    return maxim, np.array(varr)

#It returns for each key1 the time at which we reach the minimum price level and the maximum through all key1s
def warmup():
    varr = []
    for k1 in k1uniq:
        varr.append(mappk1[k1].lastminupdate)
    maxim = max(varr)
    return maxim, np.array(varr)

plots = 2


if (plots == 1):
  main()
  
  ## plot key2
  #plt.figure().patch.set_facecolor('black')
  #plt.gca().set_axis_bgcolor('black')
  #gogid = getTopId(1)
  #bns = np.unique(getid(gogid)[:,1]).size
  #histid(gogid,bins=bns)
  #plt.xlabel('price levels')
  #plt.ylabel('# trades')
  #xtks = plt.xticks()[0]
  #strtks = [  str(int(pr)) + '$' for pr in xtks ]
  #xtks = plt.xticks( xtks,strtks )
  ##plt.figure().patch.set_facecolor('black')
  #plt.savefig('pricelevels.pdf',facecolor='black')
  ##plt.close()
  #plt.clf()

  ## plot lowprice
  ##plt.xkcd()
  #plt.figure().patch.set_facecolor('black')
  #plt.gca().set_axis_bgcolor('black')
  #plt.hist(allmins(),bins=30)
  #plt.xlabel('minimum price levels')
  #plt.ylabel('# stocks')
  #xtks = plt.xticks()[0]
  #strtks = [  str(int(pr)) + '$' for pr in xtks ]
  #xtks = plt.xticks( xtks,strtks )
  #plt.savefig('minpricelevels.pdf',facecolor='black')
  ##plt.close()
  #plt.clf()
  
  ## plot max var minim
  #plt.figure().patch.set_facecolor('black')
  #plt.gca().set_axis_bgcolor('black')
  #mmx,varmin = warmup()
  #plt.hist(varmin,bins=30)
  #plt.xlabel('# trades to get lowest price')
  #plt.ylabel('# stocks')
  #xtks = plt.xticks()[0]
  ##strtks = [  str(int(pr)) + '$' for pr in xtks ]
  ##xtks = plt.xticks( xtks,strtks )
  #plt.savefig('minpriceVAR.pdf',facecolor='black')
  ##plt.close()
  #plt.clf()
  
  #plt.xkcd()
  plt.figure().patch.set_facecolor('black')
  plt.gca().set_axis_bgcolor('black')
  plt.hist(alldiffmaxminminpr()[1],bins=np.arange(alldiffmaxminminpr()[0]+2)-0.5  )
  plt.xlabel('max displacement of the minimum price after wu')
  plt.ylabel('# stocks')
  xtks = plt.xticks()[0]
  #plt.xlim(-1,9)
  strtks = [  str(int(pr)) + '$' for pr in xtks ]
  xtks = plt.xticks( np.array(xtks),strtks )
  plt.savefig('maxdisplminpricelevels.pdf',facecolor='black')
  #plt.close()
  plt.clf()

if (plots == 2):
  vals = np.array([2064.92,100.05,77.8167,46.7667,54.3333])
  xvals = np.arange(1,vals.size+1)
  names = ['original', 'opt_1' ,'opt_2','opt_3','opt_3+']
  plt.figure().patch.set_facecolor('black')
  plt.gca().set_axis_bgcolor('black')
  plt.bar( xvals , vals ,width=0.75)
  plt.xlabel('Version')
  plt.ylabel('msec')
  plt.title('Time for 10M trades',y=1.03 )
  #xtks = plt.xticks()[0]
  #plt.xlim(-1,9)
  #strtks = [  str(int(pr)) + '$' for pr in xtks ]
  plt.xlim(0.75,vals.size+1)
  xtks = plt.xticks( xvals + 0.75/2., names )
  plt.savefig('speedupALL.pdf',facecolor='black')
  #plt.close()
  plt.clf()
  
  # no original
  vals = vals[1:]
  xvals = np.arange(1,vals.size+1)
  names = names[1:]
  
  plt.figure().patch.set_facecolor('black')
  plt.gca().set_axis_bgcolor('black')
  plt.bar(xvals , vals ,width=0.75)
  plt.xlabel('Version')
  plt.ylabel('msec')
  plt.title('Time for 10M trades (opt)',y=1.03 )
  #xtks = plt.xticks()[0]
  #plt.xlim(-1,9)
  #strtks = [  str(int(pr)) + '$' for pr in xtks ]
  plt.xlim(0.75,vals.size+1)
  xtks = plt.xticks( xvals + 0.75/2., names )
  plt.savefig('speedupOPT.pdf',facecolor='black')
  #plt.close()
  plt.clf()

if(opts.INTER):
    embed()


