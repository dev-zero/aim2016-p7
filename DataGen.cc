#include <chrono>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <map>
#include <random>
#include <cstdint>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>

#include "effio.h"

// run with: g++ -O3 --std=c++11 ./example.cpp -o example && ./example

std::vector<key_value> generate_test_data(size_t count) {
  random_number_generators rngs;
  std::map<int32_t, key2_gen_state> key2_generators;
  std::vector<key_value> test_data;

  map_type_t data;

  int64_t running_total = 0;

  for (size_t i = 0; i < count; ++i)
  {
    int32_t key1 = rngs.key1();
    auto key2_gen_iter = key2_generators.find(key1);

    if (key2_gen_iter == key2_generators.end())
      key2_gen_iter = key2_generators.insert({key1, key2_gen_state{rngs}}).first;

    int32_t key2 = key2_gen_iter->second.next_value();
    auto entry = data.find(key1);

    if (entry == data.end())
      entry = data.insert({key1, {}}).first;

    auto insert_result = entry->second.insert({key2, 0});
    int32_t value = 1;

    if (insert_result.second)
    {
      insert_result.first->second = value;
    }
    else if (entry->second.size() > 1 && rngs.should_delete(insert_result.first->second))
    {
      value = -(insert_result.first->second);
      entry->second.erase(insert_result.first);
    }
    else
    {
      insert_result.first->second += value;
    }

    for (auto kv : entry->second) {}

    running_total += entry->second.begin()->second;

    test_data.push_back({key1, key2, value});
  }

  std::cout << "expected total: " << running_total << std::endl;
  return test_data;
}

int main(int argc, const char * argv[]) {
  if (argc != 3)
  {
    std::cerr << "Usage: " << argv[0] << " <number of entries> <filename>" << std::endl;
    return 1;
  }

  // Generate test data
  std::cout << "generating test data..." << std::endl;
  int SampleDataLength = std::stoi(argv[1]);
  auto test_data = generate_test_data(SampleDataLength);

  // Write data to file
  writeData2File(argv[2], test_data, SampleDataLength);
  std::cout << "DONE!\n" << std::endl;

  return 0;

}
