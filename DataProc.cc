#include <chrono>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "effio.h"

// run with: g++ -O3 --std=c++11 ./AIM_code.cc -o MyAIM_code && ./AIM_code.cc


int main(int argc, const char * argv[]) {
  if (argc != 3)
  {
    std::cerr << "Usage: " << argv[0] << " <number of entries> <filename>" << std::endl;
    return 1;
  }

  // Read test data from file
  int SampleDataLength = std::stoi(argv[1]);
  data_t test_data(SampleDataLength);
  readDataFromFile(argv[2], test_data, SampleDataLength);
  // run the algorithm, record the timings.

  // this is the bit you should try to optimise.

  std::cout << "running..." << std::endl;

  const auto begin = std::chrono::steady_clock::now();
  int64_t sum = 0;

  map_type_t data;

  // kv is a reference to a key_value instance
  for (auto& kv : test_data)
  {
    // lookup an instrument/stock, get an iterator
    auto entry = data.find(kv.key1_);

    // create an entry if not found
    if (entry == data.end())
      // map::insert() returns a tuple (iterator, bool), where .second is true
      // if this new element was actually inserted, otherwise false
      // entry is set to the newly created instance if it was data.end() before
      entry = data.insert({kv.key1_, {}}).first;

    // insert an entry with quantity 0 for this price if none exists for kv.key2_
    auto insert_result = entry->second.insert({kv.key2_, 0});

    // create a shortcut for the quantiy
    auto& value_ref = insert_result.first->second;

    // add the new quantity
    if ((value_ref += kv.value_) == 0)
      // .. and remove this quote if the quantity is 0
      entry->second.erase(insert_result.first);

    sum += entry->second.begin()->second;
  }

  const auto end = std::chrono::steady_clock::now();

  std::cout
    << "test data size: " << test_data.size()
    << " duration: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "ms,"
    << " sum: " << sum
    << std::endl;

  return 0;

}
