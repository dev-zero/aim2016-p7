/**
 * This implementation is based on the following idea:
 *
 * Since the stock id and the prices are integer numbers,
 * we can use them directly as keys to a fixed size pre-allocated array.
 * Since the data analysis shows that they are between 0 and 4096 we can
 * combine them into a 24bit key by shifting the price by 12 bits as follows:
 *   key = (price << 12) + stock
 * To get the lowest price efficiently we track them in a separate structure
 * where we employ the same idea again, with the stock id as the only key.
 *
 * We furthermore optimize it by storing the quantity in a signed char.
 *
 * This gives a O(1) lookup time for both the quantites and the lowest_price structure.
 * Calculating the next lowest price in case we cancelled out the quantity for
 * the current lowest price is most likely to be the most expensive thing in here.
 *
 * This implementation has further the disadvantage to require a large amount of memory
 * to probably store a lot of zeros and to jump around in this memory which hurts the CPU cache.
 *
 * Nevertheless, initial benchmarks show it to be 12x faster than the original code.
 */

#include <chrono>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <cassert>
#include <algorithm>
#include "effio.h"

// run with: g++ -O3 --std=c++11 ./AIM_code.cc -o MyAIM_code && ./AIM_code.cc

#if 1

// price-major data organization

const static uint32_t STOCK_SHIFT = 13; // how many bits we want to reserve for the price
const static uint32_t PRICE_SHIFT = 0; // how many bits we want to reserve for the stock

const static uint32_t LARGEST_STOCK = 2048;
const static uint32_t LARGEST_PRICE = 1 << STOCK_SHIFT;

#else

// stock-major data organization

const static uint32_t STOCK_SHIFT = 0; // how many bits we want to reserve for the price
const static uint32_t PRICE_SHIFT = 11; // how many bits we want to reserve for the stock

const static uint32_t LARGEST_STOCK = 1 << PRICE_SHIFT;
const static uint32_t LARGEST_PRICE = 8192;

#endif

// whether or not to do price shifting
#define DO_SHIFT 0

// should the Price/Stock matrix be dumped into a 'mfile.csv' at the end
#define DUMP_PS_MATRIX 0

// the type for the quantity.
// using a char for stock-major seems to be more efficient,
// while using an int16 performs 5% better for price-major
// in general: when doing shifting, using a char is always more efficient
typedef int16_t quantity_t;


int main(int argc, const char * argv[]) {
  if (argc != 3)
  {
    std::cerr << "Usage: " << argv[0] << " <number of entries> <filename>" << std::endl;
    return 1;
  }

  // Read test data from file
  int SampleDataLength = std::stoi(argv[1]);
  data_t test_data(SampleDataLength);
  readDataFromFile(argv[2], test_data, SampleDataLength);
  // run the algorithm, record the timings.

  // this is the bit you should try to optimise.

  std::cout << "running..." << std::endl;

  auto key = [] (uint32_t stock, uint32_t price) {
    return (stock << STOCK_SHIFT) + ((price % LARGEST_PRICE) << PRICE_SHIFT);
  };

  std::vector<quantity_t> quantities(LARGEST_STOCK*LARGEST_PRICE, 0);
  std::vector<uint16_t> lowest_prices(LARGEST_STOCK, LARGEST_PRICE); // initialize to always find the elements in the beginning
  std::vector<uint16_t> c_lowest_prices(LARGEST_STOCK, LARGEST_PRICE); // initialize with the largest price we can get
  int64_t sum = 0;

  const auto begin = std::chrono::steady_clock::now();

  // kv is a reference to a key_value instance
  for (auto& kv : test_data)
  {
    assert(kv.key1_ < PRICE_STRIDE && "key1_ not representable with specified number of bytes");
    assert(kv.key2_ < LARGEST_PRICE && "key2_ not representable with specified number of bytes");

    uint16_t & lowest_price = lowest_prices[kv.key1_];
    uint16_t & c_lowest_price = c_lowest_prices[kv.key1_];
    // lookup by price and instrument
    quantity_t & quantity = quantities[key(kv.key1_, kv.key2_ - lowest_price)];

    quantity += kv.value_;

    // TODO: the following decision logic could be optimized
    // further by a) doing the summation in the branches directly
    // and b) by caching also the quantity for the lowest price.
    // Furthermore it would be adviseable to also store the second
    // lowest price to be able to calculate the required answers first
    // and then do the maintenance work (finding the next second lowest
    // price in our case).

    // we get a new lowest price and since we only get non-zero
    // quantities in the quotes we can simply reset the lowest price
    if (kv.key2_ < c_lowest_price)
      c_lowest_price = kv.key2_;
    else if ((kv.key2_ == c_lowest_price) && (quantity == 0))
    {
      // this is the most expensive case:
      // we just set the available quantities for the current lowest price to zero
      // and now we have to find the next lowest price for which there is a positive quantity

      // we can directly start with the next price
      uint32_t new_lowest_price_key_offset = 1;

      // we are guaranteed to have always at least one other quote in there
      // and we have to stride over the prices array,
      // where the faster index is the stock identifier
      // the offset is based on the shift of the prices by N bytes for the combined index
      while (!quantities[key(kv.key1_, kv.key2_ - lowest_price + new_lowest_price_key_offset)])
        new_lowest_price_key_offset += 1;

      // the lowest price is the upper half of the N bit key
      // (remember: combined price/stock id key)
      c_lowest_price = kv.key2_ + new_lowest_price_key_offset;
    }

    sum += quantities[key(kv.key1_, c_lowest_price - lowest_price)];

    // Now follows some maintenance work:

#if DO_SHIFT
    // if the current lowest price is below the lowest price
    // (=the starting point of our array) we have to shift the array and
    if (c_lowest_price < lowest_price)
    {
      uint32_t shift = lowest_price - c_lowest_price;

      size_t max_s = LARGEST_PRICE / shift;

      // here we do not shift the price since we cycle the whole array
      for (size_t s = 1; s < max_s; ++s)
        for (size_t i = 0; i < shift; ++i)
          std::swap(quantities[key(kv.key1_, i)], quantities[key(kv.key1_, s*shift+i)]);
        for (size_t i = 0; i < shift; ++i)
          std::swap(quantities[key(kv.key1_, i)], quantities[key(kv.key1_, (max_s*shift+i) % LARGEST_PRICE)]);

      lowest_price = c_lowest_price;
    }
#endif

  }

  const auto end = std::chrono::steady_clock::now();

#if DUMP_PS_MATRIX
  std::ofstream mfile;
  mfile.open("mfile.csv");
  for (size_t i = 0; i < LARGEST_STOCK; ++i)
  {
    for (size_t j = 0; j < LARGEST_PRICE; ++j)
      mfile << +quantities[key(i, j)] << " ";
    mfile << std::endl;
  }
  mfile.close();
#endif

  std::cout
    << "test data size: " << test_data.size()
    << " duration: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "ms,"
    << " sum: " << sum
    << std::endl;

  return 0;

}
