#!/bin/bash

#data_length=$1
data_length=100000000

#DataFileName=$2
DataFileName='SampleData.bin'

./DataGen $data_length $DataFileName
./oBookFixedSize $data_length $DataFileName

perl -e 'sleep 1 while (!(-e $ARGV[0]))' -- "$DataFileName"

if [[ ! -d "TestData" ]]
then
    mkdir TestData
fi

mv $DataFileName TestData/$DataFileName
