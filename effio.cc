#include <fstream>
#include <vector>
#include <iterator>
#include <iostream>
#include <iomanip>
#include "effio.h"

/* MK: 
** key_value = (instrument ID, price, quantity)
** all entries are 32bit integers, price is strictly positive
**
**
** "data_t" is a new datatype that corresponds to an array containing n "key_value" vectors, where n is can be passed as an argument when the data_t variable is declared. 
*/


/**
 * Load a std::vector from a file.
 * WARNING: The user has to provide the correct number of entries.
 */

void readDataFromFile(const char* inputfilename, data_t & data, int len)
{
    std::ifstream infile;
    infile.open(inputfilename, std::fstream::in | std::fstream::binary);
    if (infile.fail())
    {
      std::cerr << "Opening file '" << inputfilename << "' failed!" << std::endl;
    }

    infile.read(reinterpret_cast<char*>(data.data()), len*sizeof(key_value));
    infile.close();
}

/**
 * A way to efficiently write a vector to a file
 */

int writeData2File(const char* outputfilename, const data_t & data, int len)
{
    std::ofstream outfile;
    outfile.open(outputfilename, std::fstream::out | std::fstream::binary | std::fstream::trunc);
    if (outfile.fail())
    {
      std::cerr << "Opening file '" << outputfilename << "' failed!" << std::endl;
      return 1;
    }

    std::cout
      << "# of entries: " << data.size()
      << std::endl;


    outfile.write(reinterpret_cast<const char*>(data.data()), data.size()*sizeof(key_value));
    outfile.close();

    return 0;
}
